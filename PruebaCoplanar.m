%% el problema de la Z coplanar
%% --> fila de ceros (perdida de dimension implicita)

p3D = load('Coordenadas3D.txt');
p2D_1 = load('Coordenadas2D_1.txt');

N = size(p3Dh,1);

ceros = [0 0 0 0];

A = zeros(N, 4*3+3);
count=0;
for k=1:N
    p3 = p3D(k,:);
    p2 = p2D_1(k,:);
    x = [p3,1, ceros, ceros, p3 .* p2(1)];
    y = [ceros, p3,1, ceros, p3 .* p2(2)];
    z = [ceros, ceros, p3,1, p3 .* -0];
    A(count+1,:) = x;
    A(count+2,:) = y;
    A(count+3,:) = z;
    count=count+3;
end


B = zeros(3*N,1);
count=0;
for k=1:N
    p2 = p2D_1(k,:);
    B(count+1,:) = p2(1);
    B(count+2,:) = p2(2);
    B(count+3,:) = 0;
    count = count+3;
end


M = pinv(A)*B;
M(16)=1;

M = reshape(M, 4,4)'
