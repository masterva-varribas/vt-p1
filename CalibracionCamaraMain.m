%% limpieza del workspace
clc
clear

%% cargamos los puntos 2D y 3D
p2D_1 = load('Coordenadas2D_1.txt');
p2D_2 = load('Coordenadas2D_2.txt');
p3D = load('Coordenadas3D.txt');


%% puntos en coordenadas homogeneas
homo = ones(size(p2D_1, 1),1);
p2Dh_1 = [p2D_1, homo]';
p2Dh_2 = [p2D_2, homo]';
p3Dh = [p3D, homo]';


%% cargamos las matrices X e Y
X1 = load('MatrizCoeficientes_1.txt');
Y1 = load('Coordenadas2Dvectorizadas_1.txt');


%% calculo de la matrix de proyeccion M
% M = (X'X)-1X' · Y == X+ · Y
X1pinv = pinv(X1);
% validacion de pinv
%X1_ = inv(X1'*X1)*X1';

M1 = X1pinv * Y1;
M1(12)=1;

%% guardamos
%https://es.mathworks.com/help/matlab/ref/save.html
save('matrixProyeccionPerspectiva1.sch', 'M1', '-ascii'); 
M1 = reshape(M1, 4,3)';

X2 = load('MatrizCoeficientes_2.txt');
Y2 = load('Coordenadas2Dvectorizadas_2.txt');

%% mismo proceso para la segunda camara...
X2pinv = pinv(X2);
M2 = X2pinv * Y2;
M2(12)=1;

save('matrixProyeccionPerspectiva2.sch', 'M2', '-ascii');
M2 = reshape(M2, 4,3)';


%%% check M 
p2Dh_1_ = M1 * p3Dh;
p2Dh_2_ = M2 * p3Dh;

%% normalizacion de la coordenada homogenea
for i = 1:10
	aux = p2Dh_1_(:,i);
	p2Dh_1_(:,i) = aux ./ aux(end);
	aux = p2Dh_2_(:,i);
	p2Dh_2_(:,i) = aux ./ aux(end);
end

%%% Calculo del error
error_1 = p2Dh_1_ - p2Dh_1;
error_2 = p2Dh_2_ - p2Dh_2;